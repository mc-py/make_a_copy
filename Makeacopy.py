import os
import configparser
from shutil import copyfile
from datetime import date
import logging
import getpass
import os.path


pathToLocalFolder = (os.path.dirname(os.path.abspath(__file__)))
pathToSourceFolder = (pathToLocalFolder+'\\FilesToExport')

config = configparser.ConfigParser()
pathToCfg = (pathToLocalFolder+'\\config.txt')

pathToLogs = (pathToLocalFolder+'\\Logs'+'\\'+str(date.today())+'_logs.txt')
if not pathToLogs:
    open(pathToLogs, "w+")

soruceFilesPathList = []
#soruceFilesNameList = []

for r, d, f in os.walk(pathToSourceFolder):
    for file in f:
        if '.' in file:
            soruceFilesPathList.append(os.path.join(r, file))
            #soruceFilesNameList.append(file)


with open(pathToCfg) as f:
    Cfg = f.read()

destPathList = Cfg.split('\n')


log_format = "%(asctime)s::%(message)s"
logging.basicConfig(filename=pathToLogs, level='INFO', format=log_format)

localUser = getpass.getuser()

logging.info('\n')
logging.info('User: "'+localUser+'" runs this script\n')


for everySourceFile in soruceFilesPathList:
    fileName = ((str(everySourceFile).split('\\'))[-1])
    for destPathEveryFile in destPathList:
        destFileName = (destPathEveryFile+"\\"+fileName)
        if os.path.isfile(destFileName):
            logging.info(' Skipping copy to: "'+
                        destFileName+
                        '" - file already exists!')
        else:
            copyfile(everySourceFile, destFileName)
            logging.info(' Copying from: "'+
                        everySourceFile+
                        '" to: "'+
                        destFileName+'"') 